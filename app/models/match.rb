class Match < ActiveRecord::Base
	has_many :players, through: :player_score
	has_many :player_scores
	accepts_nested_attributes_for :player_scores
end
