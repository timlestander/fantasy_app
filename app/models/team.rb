class Team < ActiveRecord::Base
	belongs_to :user
	has_many :players, through: :team_members
	has_many :team_members
	validates :name, presence: true
  
	def add_player(player)         
  		TeamMember.create(team: self, player: player)     
	end

	def get_points
		total = 0
    total += PlayerScore.where(player_id: self.players).sum(:goals) * 3
    total += PlayerScore.where(player_id: self.players).sum(:assists) * 2
    total += PlayerScore.where(player_id: self.players).sum(:pos)
    total -= PlayerScore.where(player_id: self.players).sum(:neg)
    total += PlayerScore.where(player_id: self.players).sum(:penalties)
    total += PlayerScore.where(player_id: self.players).sum(:mvp) * 2
    total
  end
end
