class Player < ActiveRecord::Base
	has_many :teams, through: :team_members
	has_many :team_members, dependent: :destroy
	has_many :matches, through: :player_score
	has_many :player_scores
	belongs_to :position

	validates :name, presence: true
	validates :desc, presence: true
	validates :price, presence: true
	validates :position_id, presence: true

	def total_score
		total = 0
    	total += PlayerScore.where(player_id: self.id).sum(:goals) * 3
    	total += PlayerScore.where(player_id: self.id).sum(:assists) * 2
    	total += PlayerScore.where(player_id: self.id).sum(:pos)
    	total -= PlayerScore.where(player_id: self.id).sum(:neg)
    	total -= PlayerScore.where(player_id: self.id).sum(:penalties)
    	total += PlayerScore.where(player_id: self.id).sum(:mvp) * 2
    	total
  	end
end
