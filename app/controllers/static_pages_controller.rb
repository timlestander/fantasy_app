class StaticPagesController < ApplicationController

	before_action :is_admin, only: [:admin]

  def home
  end

  def league
  	@users = User.all
  end

  def admin
  end

  def scores
  	@players = Player.all
  end

end
