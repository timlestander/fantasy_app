class PlayersController < ApplicationController

  before_action :admin_user, only: [:edit, :destroy, :update]

  def new
  	@player = Player.new
  end

  def edit
    @player = Player.find(params[:id])
  end

  def show
    @player = Player.find(params[:id])
  end

  def update
    @player = Player.find(params[:id])
    if @player.update_attributes(player_params)
      flash[:success] = "Player updated"
      redirect_to playerlist_path
    else
      render 'edit'
    end
  end

  def create
  	@player = Player.new(player_params)
  	if @player.save
      flash[:success] = "Player successfully added"
  		redirect_to new_player_path
  	else
  		flash[:danger] = "Something went wrong"
  		render 'new'
  	end
  end

  def playerlist
  	@players = Player.all
  end

  def destroy
    Player.find(params[:id]).destroy
    flash[:success] = "User deleted!"
    redirect_to playerlist_path
  end

  private

  	def player_params
  		params.require(:player).permit(:name, :desc, :price, :position_id)
  	end

    def admin_user
      unless current_user.admin?
        flash[:danger] = "Försvinn härifrån"
        redirect_to root_url
      end
    end
end
