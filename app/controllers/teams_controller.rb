class TeamsController < ApplicationController

  before_action :logged_in_user, only: [:edit, :update]
  before_action :correct_user, only: [:edit, :update]

  def new
  end

  def show
  	@user = User.find(params[:user_id])
  	@team = @user.team
  end

  def edit
    @user = User.find(params[:user_id])
    @team = @user.team
  	@players = Player.all
  end

  def update
  	player_ids = params[:players].collect {|id| id.to_i} if params[:players]
  	players = Player.where(id: player_ids)

  	if players.count > 0
  	  if players.count == 2
        if player_types(players)
  	  	  if total_price(players) <= 3
  	      	TeamMember.where(team_id: current_user.team.id).destroy_all
  	      	players.each do |player|
  	      		current_user.team.add_player(player)
  	      	end
  	      	flash[:success] = "Team updated"
  	    	else
  	      	flash[:danger] = "Your team cost too much"
  	  		end
   	      #player_ids.each { |pid| current_user.team.add_player(Player.find_by(id: pid)) }
      	else
      		flash[:danger] = "Wrong off/def numbers"
      	end
      else 
        flash[:danger] = "Please choose 5 players"
      end
  	end

  	if current_user.team.update_attributes(team_params) 
  		if flash.empty?
    		flash[:success] = "Team name updated"
    	end
  	else
  		flash[:danger] = "Please select a name for your team"
    end

  	redirect_to user_team_path
  end

  # def points(players)
  #   total = 0
  #   players.each do |player|
  #       total += PlayerScore.where(player_id: player.id).sum(:goals) * 3
  #       total += PlayerScore.where(player_id: player.id).sum(:assists) * 2
  #       total += PlayerScore.where(player_id: player.id).sum(:pos) 
  #       total -= PlayerScore.where(player_id: player.id).sum(:neg)
  #       total -= PlayerScore.where(player_id: player.id).sum(:penalties)
  #       total += PlayerScore.where(player_id: player.id).sum(:mvp) * 2
  #   end
  #   total
  # end

  private

  	def team_params
  		params.require(:team).permit(:name)
  	end

    def player_types(players)
      back = topp = 0
      players.each do |player|
        if player.position_id == 1
          back += 1
        else 
          topp += 1
        end
      end
      back == 1 and topp == 1
    end

  	def total_price(players)
  		total = 0
  		players.each do |player|
  			total += player.price.to_f
  		end
  		total
  	end
end
