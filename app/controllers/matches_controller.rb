class MatchesController < ApplicationController

	before_action :logged_in_user, only: [:new, :create, :edit]
	before_action :is_admin, only: [:new, :create, :edit]

	def new
		@players = Player.all
		@match = Match.new
	end

	def create
		match = Match.new(match_params)
		if match.save!
			flash[:success] = "Match created and added" 
		else 
			flash[:danger] = "Something went wrong"
		end
		redirect_to root_path
	end

	def index
		@matches = Match.all
	end

	def edit
		@match = Match.find(params[:id])
	end

	def update
		match = Match.find(params[:id])
		if match.save!
			match.update_attributes(update_params)
			flash[:success] = "Match updated"
		else
			flash[:danger] = "Something went wrong"
		end
		redirect_to root_path
	end

	def update_params
		params.require(:match).permit(:home_team, :away_team, :home_goal, :away_goal)
	end

	def match_params
		params.require(:match).permit(:home_team, :away_team, :home_goal, :away_goal, :date, player_scores_attributes: [:player_id, :games, :goals, :assists, :pos, :neg, :penalties, :mvp])
	end
end
