class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

    def logged_in_user
      	unless logged_in?
        	store_location
        	flash[:danger] = "Please log in"
        	redirect_to login_url
      	end
   	end

   	def correct_user
    	@user = User.find(params[:id])
    	if !current_user?(@user)
    		flash[:danger] = "You are not authorized to do that"
    		redirect_to(root_url)
    	end
   	end

    def is_admin
  		if !current_user.admin?
  			flash[:danger] = "Försök inte din lilla slynka"
  			redirect_to root_path
  		end
  	end

end
