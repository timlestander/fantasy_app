module ApplicationHelper

	def full_title(page_title = '')
		base_title = "SK IT Fantasy"
		if page_title.empty?
			base_title
		else
			page_title + " | " + base_title
		end
	end

	def current_user
		@current_user ||= User.find_by(id: session[:user_id])
	end

	def current_user?(user)
		user == current_user
	end

	 def is_admin
  		if !current_user.admin?
  			flash[:danger] = "Försök inte din lilla slynka"
  			redirect_to root_path
  		end
  	end

  	def is_admin?(user)
  		user.admin?
  	end

end
