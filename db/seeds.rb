# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:

	User.create!(name: "Tim Lestander", email: "timlestander@gmail.com", password: "foobar", password_confirmation: "foobar", admin: true)
	User.create!(name: "Random dude", email: "sug_kuk@lol.se", password: "foobar", password_confirmation: "foobar", admin: false)
	Player.create!(name: "Timp Bizkir", desc: "Bra kille", price: 3, position_id: 2)
	Player.create!(name: "Jakob Nilsson", desc: "Skön dude", price: 1.1, position_id: 1)
	Player.create!(name: "Christoffer Johansson", desc: "Reko snubbe", price: 1.9, position_id: 1)
	Position.create!(pos: "Anfall")
	Position.create!(pos: "Försvar")
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
