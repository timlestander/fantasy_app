class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :name
      t.string :desc
      t.decimal :price
      t.integer :position_id

      t.timestamps null: false
    end
  end
end
