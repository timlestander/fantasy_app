class AddRelationToTeamMemberUserTeams < ActiveRecord::Migration
  def change
  	add_column :teams, :user_id, :integer
	add_column :team_members, :team_id, :integer
	add_column :team_members, :player_id, :integer
  end
end
