class AddGamesToPlayerScore < ActiveRecord::Migration
  def change
    add_column :player_scores, :games, :integer
  end
end
