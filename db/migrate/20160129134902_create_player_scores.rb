class CreatePlayerScores < ActiveRecord::Migration
  def change
    create_table :player_scores do |t|
      t.integer :player_id
      t.integer :match_id
      t.integer :goals
      t.integer :assists
      t.integer :pos
      t.integer :neg
      t.integer :penalties
      t.integer :mvp

      t.timestamps null: false
    end
  end
end
