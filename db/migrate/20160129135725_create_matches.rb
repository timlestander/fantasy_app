class CreateMatches < ActiveRecord::Migration
  def change
    create_table :matches do |t|
      t.string :home_team, default: "TBA"
      t.string :away_team, default: "TBA"
      t.integer :home_goal, default: 0
      t.integer :away_goal, default: 0
      t.date :date

      t.timestamps null: false
    end
  end
end
